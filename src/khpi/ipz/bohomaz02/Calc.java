package khpi.ipz.bohomaz02;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * It contains the implementation of methods to compute and display the results.
 * 
 * @author bohomaz
 * @version 1.0
 */
public class Calc {

	/** The file name used in serialization. */
	private static final String FNAME = "Item2d.bin";

	/**
	 * It saves the result of the calculation. class object {@linkplain Item2d}
	 */
	private Item2d result;

	/** Initializes {@linkplain Calc#result} */
	public Calc() {
		result = new Item2d();
	}

	/**
	 * Set value {@linkplain Calc#result}
	 * 
	 * @param result
	 *            - the new value of the reference object {@linkplain Item2d}
	 */
	public void setResult(Item2d result) {

		this.result = result;
	}

	/**
	 * Set value {@linkplain Calc#result}
	 * 
	 * @return the current value of an object reference {@linkplain Item2d}
	 */
	public Item2d getResult() {
		return result;
	}

	/**
	 * It calculates the value of the function.
	 * 
	 * @param x
	 *            - The perimeter of the parallelepiped.
	 * @param y
	 *            - The area of the parallelepiped.
	 * @param z
	 *            - The volume of the parallelepiped.
	 * @return 0
	 */
	public double calc(int x, int y, int z) {
		result.setPerimeter(4 * x + 4 * y + 4 * z);
		result.setArea(2 * (x * y + y * z + x * z));
		result.setVolume(x * y * z);
		return 0;
	}

	/** It displays the result of the calculation. */
	public void show() {
		System.out.println(result);
	}

	/**
	 * Saves {@linkplain Calc#result} in file {@linkplain Calc#FNAME}
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
		os.writeObject(result);
		os.flush();
		os.close();
	}

	/**
	 * Restores {@linkplain Calc#result} from file {@linkplain Calc#FNAME}
	 * 
	 * @throws Exception
	 */
	public void restore() throws Exception {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
		result = (Item2d) is.readObject();
		is.close();
	}
}
