package khpi.ipz.bohomaz02;

import java.io.Serializable;

/**
 * Stores baseline data and calculation result.
 * 
 * @author bohomaz
 * @version 1.0
 */
public class Item2d implements Serializable {
	/** The perimeter of the parallelepiped. */
	private int perimeter;

	/** The area of the parallelepiped. */
	private int area;
	/** The volume of the parallelepiped. */
	private int volume;

	/** Automatically generated constant */
	private static final long serialVersionUID = 1L;

	/**
	 * Initializes field {@linkplain Item2d#perimeter}, {@linkplain Item2d#area}
	 * , {@linkplain Item2d#volume}
	 */
	public Item2d() {
		perimeter = 0;
		area = 0;
		volume = 0;
	}

	/**
	 * Sets the values of fields: Perimeter area and volume.
	 * 
	 * @param perimeter
	 *            - value for the initialization of the field
	 *            {@linkplain Item2d#perimeter}
	 * @param area
	 *            - value for the initialization of the field
	 *            {@linkplain Item2d#area}
	 * @param volume
	 *            - value for the initialization of the field
	 *            {@linkplain Item2d#volume}
	 */
	public Item2d(int perimeter, int area, int volume) {
		this.perimeter = perimeter;
		this.area = area;
		this.volume = volume;
	}

	/**
	 * Installation of field values {@linkplain Item2d#perimeter}
	 * 
	 * @param perimeter
	 *            - value for {@linkplain Item2d#perimeter}
	 * @return Value {@linkplain Item2d#perimeter}
	 */
	public double setPerimeter(int perimeter) {
		return this.perimeter = perimeter;
	}

	/**
	 * Installation of field values {@linkplain Item2d#perimeter}
	 * 
	 * @return Value {@linkplain Item2d#perimeter}
	 */
	public double getPerimeter() {
		return perimeter;
	}

	/**
	 * Installation of field values {@linkplain Item2d#area}
	 * 
	 * @param area
	 *            - value for {@linkplain Item2d#area}
	 * @return Value {@linkplain Item2d#area}
	 */
	public double setArea(int area) {
		return this.area = area;
	}

	/**
	 * Installation of field values {@linkplain Item2d#area}
	 * 
	 * @return Value {@linkplain Item2d#area}
	 */
	public double getArea() {
		return area;
	}

	/**
	 * Installation of field values {@linkplain Item2d#area}
	 * 
	 * @return Value {@linkplain Item2d#area}
	 */
	public double getVolume() {
		return volume;
	}

	public double setVolume(int volume) {
		return this.volume = volume;
	}

	/**
	 * Installation of values {@linkplain Item2d#perimeter} �
	 * {@linkplain Item2d#area}
	 * 
	 * @param perimeter
	 *            - value for {@linkplain Item2d#perimeter}
	 * @param area
	 *            - value for {@linkplain Item2d#area}
	 * @param volume
	 *            - value for {@linkplain Item2d#volume}
	 * @return this
	 */
	public Item2d setXYZ(int perimeter, int area, int volume) {
		this.perimeter = perimeter;
		this.area = area;
		this.volume = volume;
		return this;
	}

	/**
	 * It represents the result of calculations in a row.<br>
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "perimeter = " + perimeter + ", area = " + area + ", volume = " + volume;
	}

	/**
	 * Auto-generated method.<br>
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		Item2d other = (Item2d) obj;
		if (Double.doubleToLongBits(perimeter) != Double.doubleToLongBits(other.perimeter))
			return false;

		if (Math.abs(Math.abs(area) - Math.abs(other.area)) > .1e-10)
			return false;
		return true;
	}


}
