package khpi.ipz.bohomaz02;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;
import java.io.IOException;
import khpi.ipz.bohomaz02.Calc;

/**
 * ��������� ������������ ������������� �������.
 * 
 * @author xone
 * @version 1.0
 */
@SuppressWarnings("deprecation")
public class MainTest {
	/** �������� �������� ���������������� ������ {@linkplain Calc} */
	@Test
	public void testCalc() {
		Calc calc = new Calc();
		calc.calc(0, 0, 0);
		assertEquals(0, calc.getResult().getArea(), .1e-10);
		calc.calc(2, 2, 2);
		assertEquals(8, calc.getResult().getVolume(), .1e-10);
		calc.calc(3, 5, 7);
		assertEquals(60, calc.getResult().getPerimeter(), .1e-10);
	}

	/** �������� ������������. ������������ �������������� ������. */
	@Test
	public void testRestore() {
		Calc calc = new Calc();
		int x, y, z;
		for (int ctr = 0; ctr < 1000; ctr++) {
			x = 2;
			y = 4;
			z = 5;
			try {
				calc.save();
			} catch (IOException e) {
				Assert.fail(e.getMessage());
			}
			calc.calc(x, y, z);
			try {
				calc.restore();
			} catch (Exception e) {
				Assert.fail(e.getMessage());
			}
			assertEquals(0, calc.getResult().getArea(), .1e-10);

			assertEquals(0, calc.getResult().getVolume(), .1e-10);

			assertEquals(0, calc.getResult().getPerimeter(), .1e-10);
		}
	}
}
