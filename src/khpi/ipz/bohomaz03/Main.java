package khpi.ipz.bohomaz03;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * The calculation and display of results <br>
 * Contains the implementation of the static main () method
 * 
 * @author Bohomaz
 * @version 2.0
 * @see Main # main
 */
public class Main {
	/**
	 * An object that implements the interface {@linkplain View}; Maintains a
	 * collection of objects {@linkplain kolybelnikov_01.Item2d}
	 */
	private View view;

	/** Initializes the field {@linkplain Main # view view}. */
	public Main(View view) {
		this.view = view;
	}

	/** Displays menu. */
	protected void menu() {
		String s = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		do {
			do {
				System.out.println("Enter command...");
				System.out.print("'q'uit, 'v'iew, 'c'alc, 's'ave, 'r'estore: ");
				try {
					s = in.readLine();
				} catch (IOException e) {
					System.out.println("Error: " + e);
					System.exit(0);
				}
			} while (s.length() != 1);
			switch (s.charAt(0)) {
			case 'q':
				System.out.println("Exit.");
				break;
			case 'v':
				System.out.println("View current.");
				view.viewShow();
				break;
			case 'c':
				System.out.println("Calc.");
				view.viewInit();
				view.viewShow();
				break;
			case 's':
				System.out.println("Save current.");
				try {
					view.viewSave();
				} catch (IOException e) {
					System.out.println("Serialization error: " + e);
				}
				view.viewShow();
				break;
			case 'r':
				System.out.println("Restore last saved.");
				try {
					view.viewRestore();
				} catch (Exception e) {
					System.out.println("Serialization error: " + e);
				}
				view.viewShow();
				break;
			default:
				System.out.println("Wrong command.");
			}
		} while (s.charAt(0) != 'q');
	}

	/**
	 * Is executed when the program starts; Calls the method
	 * {@linkplain Main # menu () menu ()}
	 * 
	 * @param Args
	 *            - run the program parameters.
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableResult().getView());
		main.menu();
	}
}
