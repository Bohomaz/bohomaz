package khpi.ipz.bohomaz03;

import java.io.IOException;

/**
 * Product (Design pattern Factory Method) <br>
 * Interface "fabrikuemyh" Objects <br>
 * Declares methods Display objects
 * 
 * @author Bohomaz
 * @version 1.0
 */
public interface View {
	/** Displays the title */
	public void viewHeader();

	/** Displays the main part */
	public void viewBody();

	/** Displays the ending */
	public void viewFooter();

	/** It displays the entire object */
	public void viewShow();

	/** Initializes */
	public void viewInit();

	/** Saves data for later recovery */
	public void viewSave() throws IOException;

	/** Restores the previously saved data */
	public void viewRestore() throws Exception;
}
