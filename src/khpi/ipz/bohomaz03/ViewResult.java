package khpi.ipz.bohomaz03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import khpi.ipz.bohomaz02.Item2d;;

/**
 * ConcreteProduct (Design pattern Factory Method) <br>
 * Calculation functions Save and display results
 * 
 * @author Bohomaz
 * @version 1.0
 * @see View
 */
public class ViewResult implements View {
	/** The file name used when serializing */
	private static final String FNAME = "items.bin";
	/** Specifies the number of values for the calculation of the default */
	private static final int DEFAULT_NUM = 3;
	/** Collection calculation results */
	private ArrayList<Item2d> items = new ArrayList<Item2d>();

	/**
	 * Causes {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)} with a
	 * parameter {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM}
	 */
	public ViewResult() {
		this(DEFAULT_NUM);
	}

	/**
	 * Initializes collection {@linkplain ViewResult#items}
	 * 
	 * @param n
	 *            the initial number of elements
	 */
	public ViewResult(int n) {
		for (int ctr = 0; ctr < n; ctr++) {
			items.add(new Item2d());
		}
	}

	/**
	 * Get the value of {@linkplain ViewResult#items}
	 * 
	 * @return the current value of an object reference {@linkplain ArrayList}
	 */
	public ArrayList<Item2d> getItems() {
		return items;
	}

	/**
	 * It calculates the value of the function and stores result in the
	 * collection {@linkplain ViewResult#items}
	 * 
	 * @param x
	 *            the width
	 * @param y
	 *            the length
	 * @param z
	 *            the height
	 */
	public void calc(int x, int y, int z) {
		for (Item2d item : items) {
			item.setXYZ((4 * x + 4 * y + 4 * z), (2 * (x * y + y * z + x * z)), (x * y * z));
			x += 1;
			y += 1;
			z += 1;
		}
	}

	/**
	 * Causes <b>init(double stepX)</b> with the value of the argument<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewInit() {
		calc(14, 16, 18);
	}

	/**
	 * Implementation of the method {@linkplain View#viewShow()}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewShow() {
		viewHeader();
		viewBody();

		viewFooter();
	}

	@Override
	public void viewHeader() {
		// TODO Auto-generated method stub

	}

	@Override
	public void viewBody() {
		// TODO Auto-generated method stub

	}

	@Override
	public void viewFooter() {
		// TODO Auto-generated method stub

	}

	@Override
	public void viewSave() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void viewRestore() throws Exception {
		// TODO Auto-generated method stub

	}
}
