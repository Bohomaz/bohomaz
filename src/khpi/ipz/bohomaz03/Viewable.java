package khpi.ipz.bohomaz03;

/**
 * Creator (Design pattern Factory Method) <br>
 * Declares method "Factory" objects
 * 
 * @author Bohomaz
 * @version 1.0
 * @see Viewable # getView ()
 */
public interface Viewable {
	/** Creates an object that implements {@linkplain View} */
	public View getView();

}
