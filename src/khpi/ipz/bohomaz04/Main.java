package khpi.ipz.bohomaz04;

import khpi.ipz.bohomaz03.View;

/**
 * The calculation and display of results <br>
 * Contains the implementation of the static main () method
 * 
 * @author Bohomaz
 * @version 3.0
 * @see Main # main
 */
public class Main extends khpi.ipz.bohomaz03.Main {
	/** Initializes field {@linkplain kolybelnikov_02.Main#view view} */
	public Main(View view) {
		super(view);
	}

	/**
	 * It executed when the program starts; Calls the method
	 * {@linkplain kolybelnikov_02.Main # menu menu ()}
	 * 
	 * @param Args
	 *            - run the program parameters
	 */
	public static void main(String[] args) {
		Main main = new Main(new ViewableTable().getView());
		main.menu();
	}
}
