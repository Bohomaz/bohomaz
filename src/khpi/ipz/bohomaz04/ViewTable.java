package khpi.ipz.bohomaz04;

import java.util.Formatter;
import khpi.ipz.bohomaz02.Item2d;
import khpi.ipz.bohomaz03.ViewResult;

/**
 * ConcreteProduct (Design pattern Factory Method) <br>
 * Output in the form of a table
 * 
 * @author Bohomaz
 * @version 1.0
 * @see ViewResult
 */

public class ViewTable extends ViewResult {
	/** Specifies the width of the default table */
	private static final int DEFAULT_WIDTH = 30;
	/** The current width of the table */
	private int width;

	/**
	 * Set {@linkplain ViewTable # width width} Value
	 * {@linkplain ViewTable # DEFAULT_WIDTH DEFAULT_WIDTH} <br>
	 * Called by the constructor of the superclass
	 * {@linkplain ViewResult # ViewResult () ViewResult ()}
	 */
	public ViewTable() {
		width = DEFAULT_WIDTH;
	}

	/**
	 * Set {@linkplain ViewTable # width} value <b> width </ b> <br>
	 * Called by the constructor of the superclass
	 * {@linkplain ViewResult # ViewResult () ViewResult ()}
	 * 
	 * @param Width
	 *            determines the width of the table
	 */
	public ViewTable(int width) {
		this.width = width;
	}

	/**
	 * Set {@linkplain ViewTable # width} value <b> width </ b> <br>
	 * Called by the constructor of the superclass
	 * {@linkplain ViewResult # ViewResult (int n) ViewResult (int n)}
	 * 
	 * @param Width
	 *            determines the width of the table
	 * @param N
	 *            the number of items in the collection; transmitted
	 *            superconstructor
	 */
	public ViewTable(int width, int n) {
		super(n);
		this.width = width;
	}

	/**
	 * Sets the field {@linkplain ViewTable # width} value <b> width </ b>
	 * 
	 * @param Width
	 *            the width of the new table
	 * @return The specified parameter <b> width </ b> table width
	 */
	public int setWidth(int width) {
		return this.width = width;
	}

	/**
	 * Returns the value of the field {@linkplain ViewTable # width}
	 * 
	 * @return The current width of the table
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Displays the width of the vertical divider {@linkplain ViewTable # width}
	 * characters
	 */
	private void outLine() {
		for (int i = width; i > -1; i--) {
			System.out.print('-');
		}
	}

	/**
	 * Raises {@linkplain ViewTable # outLine ()}; completes the output line
	 * delimiter
	 */
	private void outLineLn() {
		outLine();
		System.out.println();
	}

	/**
	 * Displays the table header width {@linkplain ViewTable # width} characters
	 */
	private void outHeader() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%d%s%2$d%s", "%", (width - 4) / 3, "s| %", (width - 4) / 3, "s |  %", "s\n");
		System.out.printf(fmt.toString(), "Perimeter ", "Area ", "Volume ");
	}

	/**
	 * Displays the body of the table width {@linkplain ViewTable # width}
	 * characters
	 */
	private void outBody() {
		Formatter fmt = new Formatter();
		fmt.format("%s%d%s%d%s%2$d%s", "%", (width - 4) / 3, "s  | %", (width - 4) / 3, "s | %", "s\n");
		for (Item2d item : getItems()) {
			System.out.printf(fmt.toString(), item.getPerimeter(), item.getArea(), item.getVolume());
		}
	}

	/**
	 * Overloading (alignment, overloading) superclass method; Sets the field
	 * {@linkplain ViewTable # width} value <b> width </ b> <br>
	 * It is the method {@linkplain ViewResult # viewInit () viewInit ()}
	 * 
	 * @param Width
	 *            the width of the new table
	 */
	public final void calc(int width) { // method overloading
		this.width = width;
		viewInit();
	}

	/**
	 * Overloading superclass method; Sets the field
	 * {@linkplain ViewTable # width} value <b> width </ b> <br>
	 * For an object {@linkplain ViewTable} calls the method
	 * {@linkplain ViewTable # init (double stepX)}
	 * 
	 * @param Width
	 *            the width of the new table.
	 * @param x
	 *            passed to the method <b> calc (int) </ b>
	 * @param y
	 *            passed to the method <b> calc (int) </ b>
	 * @param z
	 *            passed to the method <b> calc (int) </ b>
	 */
	public final void calc(int width, int x, int y, int z) { // method
																// overloading
		this.width = width;
		calc(x, y, z);
	}

	/**
	 * ��������������� (���������, overriding) ������ �����������; �������
	 * �������������� ��������� � �������� ����� �����������
	 * {@linkplain ViewResult#calc(int x) calc(int x)}<br>
	 * {@inheritDoc}
	 */
	@Override
	public void calc(int x, int y, int z) { // method overriding
		System.out.print("Calculation... ");
		super.calc(x, y, z);
		System.out.println("done. ");
	}

	/**
	 * The output element of the table<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewHeader() {
		outHeader();
		outLineLn();
	}

	/**
	 * The output element of the table<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewBody() {
		outBody();
	}

	/**
	 * The output element of the table<br>
	 * {@inheritDoc}
	 */
	@Override
	public void viewFooter() {
		outLineLn();
	}
}
