package khpi.ipz.bohomaz04;

import khpi.ipz.bohomaz03.View;
import khpi.ipz.bohomaz03.Viewable;

/**
 * ConcreteCreator (Design pattern Factory Method) <br>
 * Declares method "Factory" objects
 * 
 * @author Bohomaz
 * @version 1.0
 * @see Viewable
 * @see ViewableResult # getView ()
 */
public class ViewableResult implements Viewable {
	/** Creates a display object {@linkplain ViewResult} */
	@Override
	public View getView() {
		return new ViewResult();
	}

}
