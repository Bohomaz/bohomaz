package khpi.ipz.bohomaz04;

import khpi.ipz.bohomaz03.ViewableResult;
import khpi.ipz.bohomaz03.View;

/**
 * ConcreteCreator (Design pattern Factory Method) <br>
 * Declares method "Factory" objects
 * 
 * @author Kolybelnikov
 * @version 2.0
 * @see ViewableResult
 * @see ViewableTable # getView ()
 */
public class ViewableTable extends ViewableResult {
	/** Create a display object {@linkplain ViewTable} */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
