package khpi.ipz.bohomaz05;

import khpi.ipz.bohomaz03.View;
import khpi.ipz.bohomaz04.ViewableTable;
import khpi.ipz.bohomaz05.ChangeConsoleCommand;
import khpi.ipz.bohomaz05.GenerateConsoleCommand;
import khpi.ipz.bohomaz05.RestoreConsoleCommand;
import khpi.ipz.bohomaz05.SaveConsoleCommand;
import khpi.ipz.bohomaz05.ViewConsoleCommand;
import khpi.ipz.bohomaz08.ViewWindow;

/**
 * ��������� � ���������� ����; ��������� ������ Singleton
 * 
 * @author xone
 * @version 1.0
 */
public class Application {
	/**
	 * ������ �� ��������� ������ Application; ������ Singleton
	 * 
	 * @see Application
	 */
	private static Application instance = new Application();

	/**
	 * �������� �����������; ������ Singleton
	 * 
	 * @see Application
	 */
	private Application() {
	}

	/**
	 * ���������� ������ �� ��������� ������ Application; ������ Singleton
	 * 
	 * @see Application
	 */
	public static Application getInstance() {
		return instance;
	}

	/**
	 * ������, ����������� ��������� {@linkplain View}; ����������� ���������
	 * �������� {@linkplain ex01.Item2d}; ���������������� � ������� Factory
	 * Method
	 */
	private View view = new ViewableTable().getView();
	/**
	 * ������ ������ {@linkplain Menu}; ������������ (������ Command)
	 */
	private Menu menu = new Menu();

	/**
	 * ��������� ������ ������������
	 * 
	 * @see Application
	 */
	public void run(String isWindow) {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
	
		menu.add(new ChangeConsoleCommand(view));
		menu.add(new SaveConsoleCommand(view));
		menu.add(new RestoreConsoleCommand(view));
		
		if(isWindow == "init window") {
			ViewWindow window = new ViewWindow();
			window.viewInit();
			window.viewShow();
		}
		
		menu.execute();
	}
}
