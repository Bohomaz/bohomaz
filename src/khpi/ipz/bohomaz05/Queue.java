package khpi.ipz.bohomaz05;



/**
 * ������������ ������ ��� ��������� � ���������� ����� ������������ ������;
 * ������ Worker Thread
 * 
 * @author Bohomaz
 * @version 1.0
 * @see Command
 */
public interface Queue {
	/**
	 * ��������� ����� ������ � �������; ������ Worker Thread
	 * 
	 * @param cmd
	 *            ������
	 */
	void put(Command cmd);

	/**
	 * ������� ������ �� �������; ������ Worker Thread
	 * 
	 * @return ��������� ������
	 */
	Command take();
}
