package khpi.ipz.bohomaz05;

import khpi.ipz.bohomaz03.View;

/**
 * ���������� ������� Restore; ������ Command
 * 
 * @author xone
 * @version 1.0
 */
public class RestoreConsoleCommand implements ConsoleCommand {

	private View view; 
	/**
	 * ������, ����������� ��������� {@linkplain View}; ��� private View view;
	 * 
	 *  �������������� ���� {@linkplain RestoreConsoleCommand#view}
	 * 
	 * @param view
	 *            ������, ����������� ��������� {@linkplain View}
	 */
	public RestoreConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'r';
	}

	@Override
	public String toString() {
		return "'r'estore";
	}

	@Override
	public void execute() {
		System.out.println("Restore last saved.");
		try {
			view.viewRestore();
		} catch (Exception e) {
			System.err.println("Serialization error: " + e);
		}
		view.viewShow();
	}
}
