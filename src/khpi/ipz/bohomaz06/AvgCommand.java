package khpi.ipz.bohomaz06;

import java.util.concurrent.TimeUnit;
import khpi.ipz.bohomaz02.Item2d;
import khpi.ipz.bohomaz03.ViewResult;
import khpi.ipz.bohomaz05.Command;

/**
 * ������, ������������ ������������ ������; ������ Worker Thread
 * 
 * @author bohomaz
 * @version 1.0
 * @see Command
 * @see CommandQueue
 */
public class AvgCommand implements Command /* , Runnable */ {
	/** ������ ��������� ��������� ��������� */
	private double result = 0.0;
	/** ���� ���������� ���������� */
	private int progress = 0;
	/** ����������� ��������� �������� {@linkplain ex01.Item2d} */
	private ViewResult viewResult;

	/**
	 * ���������� ���� {@linkplain MaxCommand#viewResult}
	 * 
	 * @return �������� {@linkplain MaxCommand#viewResult}
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}

	/**
	 * ������������� ���� {@linkplain MaxCommand#viewResult}
	 * 
	 * @param viewResult
	 *            �������� ��� {@linkplain MaxCommand#viewResult}
	 * @return ����� �������� {@linkplain MaxCommand#viewResult}
	 */
	public ViewResult setViewResult(ViewResult viewResult) {
		return this.viewResult = viewResult;
	}

	/**
	 * �������������� ���� {@linkplain MaxCommand#viewResult}
	 * 
	 * @param viewResult
	 *            ������ ������ {@linkplain ViewResult}
	 */
	public AvgCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * ���������� ���������
	 * 
	 * @return ���� {@linkplain MaxCommand#result}
	 */
	public double getResult() {
		return result;
	}

	/**
	 * ��������� ���������� ����������
	 * 
	 * @return false - ���� ��������� ������, ����� - true
	 * @see MaxCommand#result
	 */
	public boolean running() {
		return progress < 100;
	}

	/**
	 * ������������ ������������ ������ {@linkplain CommandQueue}; ������ Worker
	 * Thread
	 */
	@Override
	public void execute() {
		progress = 0;
		System.out.println("Average executed...");
		result = 0.0;
		int idx = 1, size = viewResult.getItems().size();
		for (Item2d item : viewResult.getItems()) {
			result += item.getPerimeter();
			progress = idx * 100 / size;

			try {
				TimeUnit.MILLISECONDS.sleep(2000 / size);
			} catch (InterruptedException e) {
				System.err.println(e);
			}
		}
		result /= size;
		System.out.println("Average done. Result = " + String.format("%.2f", result));
		progress = 100;
	}
	/**
	 * @Override public void run() { execute(); } /
	 **/
}
