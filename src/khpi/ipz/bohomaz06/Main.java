package khpi.ipz.bohomaz06;

import khpi.ipz.bohomaz03.View;
import khpi.ipz.bohomaz03.ViewableResult;
import khpi.ipz.bohomaz05.ChangeConsoleCommand;
import khpi.ipz.bohomaz05.GenerateConsoleCommand;
import khpi.ipz.bohomaz05.Menu;
import khpi.ipz.bohomaz05.ViewConsoleCommand;


/**
 * ���������� � ����������� �����������; �������� ���������� ������������ ������
 * main()
 * 
 * @author bohomaz
 * @version 5.0
 * @see Main#main
 */
public class Main {
	/**
	 * ������, ����������� ��������� {@linkplain View}; ����������� ���������
	 * �������� {@linkplain ex01.Item2d}; 15 ���������������� � ������� Factory
	 * Method
	 */
	private View view = new ViewableResult().getView();
	/**
	 * ������ ������ {@linkplain Menu}; ������������ (������ Command)
	 */
	private Menu menu = new Menu();

	/** ��������� ������ ������������ */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new ChangeConsoleCommand(view));
		menu.add(new ExecuteConsoleCommand(view));
		menu.execute();
	}

	/**
	 * ����������� ��� ������� ���������
	 * 
	 * @param args
	 *            ��������� ������� ���������
	 */
	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}
}
