package khpi.ipz.bohomaz06;

import khpi.ipz.bohomaz05.Command;

/**
 * ������������ ������ ��� ��������� � ���������� ����� ������������ ������;
 * ������ Worker Thread
 * 
 * @author bohomaz
 * @version 1.0
 * @see Command
 */
public interface Queue {
	/**
	 * ��������� ����� ������ � �������; ������ Worker Thread
	 * 
	 * @param cmd
	 *            ������
	 */
	void put(Command cmd);

	/**
	 * ������� ������ �� �������; ������ Worker Thread
	 * 
	 * @return ��������� ������
	 */
	Command take();
}
