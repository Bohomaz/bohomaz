package khpi.ipz.bohomaz07;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * The base class observer, Use annotations To determine the methods, Processing
 * some developments; Observer pattern
 * 
 * @author bohomaz
 * @version 1.0
 * @see Observer
 * @see Observable
 */
public abstract class AnnotatedObserver implements Observer {
	/**
	 * Associative array of event handlers; includes a pair of event handlers
	 */
	private Map<Object, Method> handlers = new HashMap<Object, Method>();

	/**
	 * Fills {@linkplain AnnotatedObserver # handlers} reference to the methods,
	 * Marked annotation {@linkplain Event}
	 */
	public AnnotatedObserver() {
		for (Method m : this.getClass().getMethods()) {
			if (m.isAnnotationPresent(Event.class)) {
				handlers.put(m.getAnnotation(Event.class).value(), m);
			}
		}
	}

	@Override
	public void handleEvent(Observable observable, Object event) {
		Method m = handlers.get(event);
		try {
			if (m != null)
				m.invoke(this, observable);
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
