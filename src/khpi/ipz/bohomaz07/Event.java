package khpi.ipz.bohomaz07;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Abstract time Time for destination Observer methods Particular events
 * 
 * @author Bohomaz
 * @see AnnotatedObserver
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
	String value();
}
