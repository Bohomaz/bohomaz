package khpi.ipz.bohomaz07;

/**
 * Defines element collection
 * 
 * @author bohomaz
 * @see Items
 */
public class Item implements Comparable<Item> {
	/** Information field */
	private String data;

	/**
	 * Initializes {@linkplain Item # data}
	 * 
	 * @param Data
	 *            value for the field {@linkplain Item # data}
	 */
	public Item(String data) {
		this.data = data;
	}

	/**
	 * Sets the field {@linkplain Item # data}
	 * 
	 * @param Data
	 *            value for the field {@linkplain Item # data}
	 * @return The value of the field {@linkplain Item # data}
	 */
	public String setData(String data) {
		return this.data = data;
	}

	/**
	 * Returns the field {@linkplain Item # data}
	 * 
	 * @return The value of the field {@linkplain Item # data}
	 */
	public String getData() {
		return data;
	}

	@Override
	public int compareTo(Item o) {
		return data.compareTo(o.data);
	}

	@Override
	public String toString() {
		return data;
	}
}
