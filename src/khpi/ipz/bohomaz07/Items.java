package khpi.ipz.bohomaz07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Container facilities; Observed object; Pattern Observer
 * 
 * @author bohomaz
 * @see Observable
 * @see Observer
 * @see Item
 */
public class Items extends Observable implements Iterable<Item> {
	/** The constant identifier events processed by observers */
	public static final String ITEMS_CHANGED = "ITEMS_CHANGED";
	/** The constant identifier events processed by observers */
	public static final String ITEMS_EMPTY = "ITEMS_EMPTY";
	/** The constant identifier events processed by observers */
	public static final String ITEMS_REMOVED = "ITEMS_REMOVED";
	/** Collection object class {@linkplain Item} */
	private List<Item> items = new ArrayList<Item>();

	/**
	 * Adds an object to the collection and notifies observers
	 * 
	 * @param Item
	 *            object of class {@linkplain Item}
	 */
	public void add(Item item) {
		items.add(item);
		if (item.getData().isEmpty())
			call(ITEMS_EMPTY);
		else
			call(ITEMS_CHANGED);
	}

	/**
	 * Adds an object to the collection
	 * 
	 * @param S
	 *            passed to the constructor {@linkplain Item # Item (String)}
	 */
	public void add(String s) {
		add(new Item(s));
	}

	/**
	 * Adds more objects in the collection and notifies observers
	 * 
	 * @param N
	 *            the number of added objects class {@linkplain Item}
	 */
	public void add(int n) {
		if (n > 0) {
			while (n-- > 0)
				items.add(new Item(""));
			call(ITEMS_EMPTY);
		}
	}

	/**
	 * Removes an object from the collection and notifies observers
	 * 
	 * @param Item
	 *            deleted object
	 */
	public void del(Item item) {
		if (item != null) {
			items.remove(item);
			call(ITEMS_REMOVED);
		}
	}

	/**
	 * Removes an object from the collection and notifies observers
	 * 
	 * @param Index
	 *            index of the deleted object
	 */
	public void del(int index) {
		if ((index >= 0) && (index < items.size())) {
			items.remove(index);
			call(ITEMS_REMOVED);
		}
	}

	/**
	 * Returns a reference to a collection
	 * 
	 * @return A reference to a collection of objects of class {@linkplain Item}
	 */
	public List<Item> getItems() {
		return items;
	}

	@Override
	public Iterator<Item> iterator() {
		return items.iterator();
	}
}
