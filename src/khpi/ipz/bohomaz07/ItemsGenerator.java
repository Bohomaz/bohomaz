package khpi.ipz.bohomaz07;

/**
 * Observer; Defines methods Event; Uses the Event; Pattern Observer
 * 
 * @author Bohomaz
 * @see AnnotatedObserver
 * @see Event
 */

public class ItemsGenerator extends AnnotatedObserver {
	/**
	 * An event handler {@linkplain Items # ITEMS_EMPTY}; Informs observers;
	 * Observer pattern
	 * 
	 * @param Observable
	 *            observable object of class {@linkplain Items}
	 * @see Observable
	 */
	@Event(Items.ITEMS_EMPTY)
	public void itemsEmpty(Items observable) {
		for (Item item : observable) {
			if (item.getData().isEmpty()) {
				int len = (int) (Math.random() * 10) + 1;
				String data = "";
				for (int n = 1; n <= len; n++) {
					data += ('A');
				}
				item.setData(data);

			}
		}
		observable.call(Items.ITEMS_CHANGED);
	}
}
