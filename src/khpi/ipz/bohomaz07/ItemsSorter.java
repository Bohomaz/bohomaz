package khpi.ipz.bohomaz07;

import java.util.Collections;

/**
 * Observer; Defines methods Event; Uses the Event; Pattern Observer
 * 
 * @author Bohomaz
 * @see AnnotatedObserver
 * @see Event
 */
public class ItemsSorter extends AnnotatedObserver {
	/** The constant identifier events processed by observers */
	public static final String ITEMS_SORTED = "ITEMS_SORTED";

	/**
	 * An event handler {@linkplain Items # ITEMS_CHANGED}; Informs observers;
	 * Observer pattern
	 * 
	 * @param Observable
	 *            observable object of class {@linkplain Items}
	 * @see Observable
	 */
	@Event(Items.ITEMS_CHANGED)
	public void itemsChanged(Items observable) {
		Collections.sort(observable.getItems());
		observable.call(ITEMS_SORTED);
	}

	/**
	 * An event handler {@linkplain Items # ITEMS_SORTED}; Observer pattern
	 * 
	 * @param Observable
	 *            observable object of class {@linkplain Items}
	 * @see Observable
	 */
	@Event(ITEMS_SORTED)
	public void itemsSorted(Items observable) {
		System.out.println(observable.getItems());
	}

	/**
	 * An event handler {@linkplain Items # ITEMS_REMOVED}; Observer pattern
	 * 
	 * @param Observable
	 *            observable object of class {@linkplain Items}
	 * @see Observable
	 */
	@Event(Items.ITEMS_REMOVED)
	public void itemsRemoved(Items observable) {
		System.out.println(observable.getItems());
	}
}
