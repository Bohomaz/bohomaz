package khpi.ipz.bohomaz07;

import khpi.ipz.bohomaz05.ConsoleCommand;
import khpi.ipz.bohomaz05.Menu;

/**
 * Implements dialogue The user; Contains a static Main () method
 * 
 * @author bohomaz
 * @version 6.0
 * @see Main # main
 */
public class Main {
	/**
	 * A console command; Is used for The creation of anonymous Command copies
	 * custom Interface; ������ Command
	 * 
	 * @author bohomaz
	 * @see ConsoleCommand
	 */
	abstract class ConsoleCmd implements ConsoleCommand {
		/** A collection of objects {@linkplain Items} */
		protected Items items;
		/** Display name of the team */
		private String name;
		/** Character hot key commands */
		private char key;

		/**
		 * Initializes console command field
		 * 
		 * @param items
		 *            {@linkplain ConsoleCmd#items}
		 * @param name
		 *            {@linkplain ConsoleCmd#name}
		 * @param key
		 *            {@linkplain ConsoleCmd#key}
		 */
		ConsoleCmd(Items items, String name, char key) {
			this.items = items;
			this.name = name;
			this.key = key;
		}

		@Override
		public char getKey() {
			return key;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	/**
	 * Sets Us observer with the observed objects; Implements the user dialogue
	 */
	public void run() {
		Items items = new Items();
		ItemsGenerator generator = new ItemsGenerator();
		ItemsSorter sorter = new ItemsSorter();
		items.addObserver(generator);
		items.addObserver(sorter);
		Menu menu = new Menu();
		menu.add(new ConsoleCmd(items, "'v'iew", 'v') {
			@Override
			public void execute() {
				System.out.println(items.getItems());
			}
		});
		menu.add(new ConsoleCmd(items, "'a'dd", 'a') {
			@Override
			public void execute() {
				items.add("");
			}
		});
		menu.add(new ConsoleCmd(items, "'d'el", 'd') {
			@Override
			public void execute() {
				items.del((int) Math.round(Math.random() * (items.getItems().size() - 1)));
			}
		});
		menu.execute();
	}

	/**
	 * It is running at startup
	 * 
	 * @param Args
	 *            the parameters start the program
	 */
	public static void main(String[] args) {
		new Main().run();
	}
}
