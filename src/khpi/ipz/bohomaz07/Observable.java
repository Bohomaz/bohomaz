package khpi.ipz.bohomaz07;

import java.util.HashSet;
import java.util.Set;

/**
 * Defines means cooperation observers And observed; Pattern Observer
 * 
 * @author bohomaz
 * @version 1.0
 * @see Observer
 */
public abstract class Observable {
	/**
	 * �a lot of observers; Observer pattern
	 * 
	 * @see Observer
	 */
	private Set<Observer> observers = new HashSet<Observer>();

	/**
	 * Adds an observer; Observer pattern
	 * 
	 * @param Observer
	 *            object-observer
	 */
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	/**
	 * Deletes an observer; Observer pattern
	 * 
	 * @param Observer
	 *            object-observer
	 */
	public void delObserver(Observer observer) {
		observers.remove(observer);
	}

	/**
	 * Notifies the observer of the event; Observer pattern
	 * 
	 * @param Event
	 *            the event information
	 */
	public void call(Object event) {
		for (Observer observer : observers) {
			observer.handleEvent(this, event);
		}
	}
}
