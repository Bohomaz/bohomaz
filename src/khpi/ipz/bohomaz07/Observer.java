package khpi.ipz.bohomaz07;

/**
 * Represents the method To communicate Monitored object And the observer;
 * Pattern Observer
 * 
 * @author bohomaz
 * @version 1.0
 * @see Observable
 */
public interface Observer {
	/**
	 * Called the observed object for each observer; Observer pattern
	 * 
	 * @param Observable
	 *            link to the observed object
	 * @param Event
	 *            the event information
	 */
	public void handleEvent(Observable observable, Object event);
}
