package khpi.ipz.bohomaz08;

import khpi.ipz.bohomaz03.ViewResult;
import khpi.ipz.bohomaz05.Application;

/**
 * The calculation and display Results; Contains implementation Static method
 * main ()
 * 
 * @author bohomaz
 * @version 7.0
 * @see Main#main
 */
public class Main {
	/**
	 * It executed when the program starts; Calls the method
	 * {@linkplain Application # run (Viewable)}
	 * 
	 * @param Args
	 *            the parameters start the program
	 */
	public static void main(String[] args) {
		Application app = Application.getInstance();	
		app.run("init window");
		System.exit(0);
	}
}
