package khpi.ipz.bohomaz08;

import java.awt.Dimension;
import khpi.ipz.bohomaz03.ViewResult;

/**
 * ConcreteProduct (Design pattern Factory Method); Display graphics
 * 
 * @author Bohomaz
 * @version 1.0
 * @see ViewResult
 * @see Window
 */
public class ViewWindow extends ViewResult {

	/** Number of items in the collection */
	private static final int POINTS_NUM = 3;
	/** display window */
	private Window window = null;

	/** Create and display windows */
	public ViewWindow() {
		super(POINTS_NUM);
		window = new Window(this);
		window.setSize(new Dimension(640, 480));
		window.setTitle("Result");
		window.setVisible(true);
	}

	public void viewInit() {
		calc(17, 3, 22);
	}

	@Override
	public void viewShow() {
		super.viewShow();
		window.setVisible(true);
		window.repaint();
	}
}
