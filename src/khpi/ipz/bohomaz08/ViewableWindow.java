package khpi.ipz.bohomaz08;

import khpi.ipz.bohomaz03.View;
import khpi.ipz.bohomaz03.Viewable;
/** ConcreteCreator
  * (Design pattern
  * Factory Method);
  * Implements the method,
  * "Factory" objects
  * @author bohomaz
  * @version 1.0
  * @see Viewable
  * @see Viewable # getView ()
*/
public class ViewableWindow implements Viewable {
@Override
public View getView() {
 return new ViewWindow();
}
}

