package khpi.ipz.bohomaz08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import khpi.ipz.bohomaz02.Item2d;
import khpi.ipz.bohomaz03.ViewResult;

/**
 * Creating a window Display graphics
 * 
 * @author bohomaz
 * @version 1.0
 * @see Frame
 */
@SuppressWarnings("serial")
public class Window extends Frame {
	/** The thickness of the padding on the edge of the window */
	private static final int BORDER = 20;
	/**
	 * An object that implements the interface {@linkplain kolybelnikov_02.View}
	 * ; <br>
	 * Maintains a collection of objects {@linkplain kolybelnikov_01.Item2d}
	 */

	private ViewResult view;

	/**
	 * Initializes {@linkplain Window # view}; <br>
	 * Creates a handler for the Close event
	 * 
	 * @param View
	 *            the value for the field {@linkplain Window # view}
	 */
	public Window(ViewResult view) {
		this.view = view;
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				// System.exit(0);
				setVisible(false);
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		Rectangle r = getBounds(), c = new Rectangle();
		r.x += BORDER;
		r.y += 25 + BORDER;
		r.width -= r.x + BORDER;
		r.height -= r.y + BORDER;
		c.x = r.x;
		c.y = r.y + r.height / 2;
		c.width = r.width;
		c.height = r.height / 2;
		g.setColor(Color.LIGHT_GRAY);
		g.setColor(Color.RED);
		g.drawLine(c.x, c.y, c.x + c.width, c.y);
		g.drawLine(c.x, r.y, c.x, r.y + r.height);
		double maxX = 0, maxY = view.getItems().get(0).getArea(), scaleX, scaleY;
		for (Item2d item : view.getItems()) {
			if (item.getPerimeter() > maxX)
				maxX = item.getPerimeter();
			if (Math.abs(item.getArea()) > maxY)
				maxY = Math.abs(item.getArea());
		}
		g.drawString("+" + maxY, r.x, r.y);
		g.drawString("-" + maxY, r.x, r.y + r.height);
		g.drawString("+" + maxX, c.x + c.width - g.getFontMetrics().stringWidth("+" + maxX), c.y);
		scaleX = c.width / maxX;
		scaleY = c.height / maxY;
		g.setColor(Color.BLUE);
		for (Item2d item : view.getItems()) {
			g.drawOval(c.x + (int) (item.getPerimeter() * scaleX) - 5, c.y - (int) (item.getArea() * scaleY) - 5, 10,
					10);
		}
	}
}
